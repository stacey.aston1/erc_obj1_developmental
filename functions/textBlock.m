function keyCode = textBlock(params,str)

% Show text
Screen('FillRect', params.window, params.waterColour);
DrawFormattedText(params.window, str,...
    'center','center',...
    params.textColour);
Screen('Flip', params.window);

% wait for a response
[~,~,keyCode] = KbCheck;
while ~params.isContinue(keyCode) && ~params.isQuit(keyCode)
    [~,~,keyCode] = KbCheck;
end

% make sure button is realeased before continuing
while params.isContinue(keyCode) && ~params.isQuit(keyCode)
    [~,~,keyCode] = KbCheck;
end

end