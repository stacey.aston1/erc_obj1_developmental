function [data,nCorrect] = runMainTrial(params,data,nCorrect,t)

% wait until no mouse buttons are pressed,
% to avoid re-registering the same press
[mx,~,buttons] = GetMouse;
[~,~,keyCode] = KbCheck;
while ~params.isQuit(keyCode) && any(buttons)
    [mx,~,buttons] = GetMouse;
    [~,~,keyCode] = KbCheck;
end

% a variable that keeps track of whether there has been a response yet
response = 0; % 0 = no response, 1 = response but no feedback yet, 2 = trial over

% keep reading the mouse and updating the screen until there has been a
% response
tic;
while response < 2 && ~params.isQuit(keyCode)
    
    % get the dot positions
    dotXpos = data.trials(t,4:3+params.nDots);
    dotYpos = data.trials(t,4+params.nDots:3+params.nDots*2);
    
    % make the dot rects
    dotRects = nan(4,length(dotXpos));
    if data.trials(t,3) == 1
        for l = 1:length(dotXpos)
            dotRects(:,l) = [dotXpos(1,l)*params.screenXpixels-params.dotSizePix;
                dotYpos(1,l)*params.screenYpixels-params.dotSizePix;
                dotXpos(1,l)*params.screenXpixels+params.dotSizePix;
                dotYpos(1,l)*params.screenYpixels+params.dotSizePix];
        end
    else
        bs = data.trials(t,4+2*params.nDots:3+3*params.nDots); % vertical axes
        as = (params.dotSizePix^2)./bs; % horizontal axes
        for l = 1:length(dotXpos)
            dotRects(:,l) = [dotXpos(1,l)*params.screenXpixels-as(1,l);
                dotYpos(1,l)*params.screenYpixels-bs(1,l);
                dotXpos(1,l)*params.screenXpixels+as(1,l);
                dotYpos(1,l)*params.screenYpixels+bs(1,l)];
        end
    end
    
    % get the true position
    trueXpos = data.trials(t,2);
    
    % set the background colour
    Screen('FillRect', params.window, params.waterColour);
    
    % draw the bucket
    Screen('DrawTexture', params.window, params.imTexBu, [], params.buckRect, 0);
    
    % draw the tenticles
    if data.trials(t,3) > 1
        % ovals
        Screen('FillOval', params.window, params.cueColour, dotRects);
    else
        % squares
        Screen('FillRect', params.window, params.cueColour, dotRects);
    end
    
    % the net
    Screen('DrawLine', params.window, params.lineColour, mx, params.yCenter - 2.5*params.dotSizePix,...
        mx, params.yCenter + 2.5*params.dotSizePix, params.lineWidth);
    
    % the correct location (only if response is issued, i.e. we are giving
    % feedback)
    if response
        
        % record this repsonse
        data.trials(t,end-1) = clickedPos;
        data.trials(t,end) = toc;
        
        % draw the "target"
        if data.part.mapDir == 2
            trueb = max(params.shapefirstLastCues)-trueXpos*params.totalShape;
        else
            trueb = trueXpos*params.totalShape+min(params.shapefirstLastCues);
        end
        truea = (params.dotSizePix^2)./trueb;
        targetRect = [trueXpos*params.screenXpixels-truea;
            params.yCenter - 2.5*params.dotSizePix-trueb*3;
            trueXpos*params.screenXpixels+truea;
            params.yCenter - 2.5*params.dotSizePix-trueb];
        Screen('FillOval', params.window, params.feedbackColour, targetRect);
        
        % show the octopus
        rect = [trueXpos.*params.screenXpixels-size(params.octopus,2)*params.oScale,...
            params.yCenter-params.nDots*params.dotSizePix*3-size(params.octopus,1)*2*params.oScale,...
            trueXpos.*params.screenXpixels+size(params.octopus,2)*params.oScale,...
            params.yCenter-params.nDots*params.dotSizePix*3];
        Screen('DrawTexture', params.window, params.imTexW, [], rect, 0);
        
        response = 2;
        
    end
    % show it
    Screen('Flip', params.window);
    
    % read the mouse and keyboard to see if we have a response or are
    % quiting
    [mx,~,buttons] = GetMouse;
    [~,~,keyCode] = KbCheck;
    if any(buttons) && ~response
        response = 1;
        clickedPos = mx/params.screenXpixels;
    end
    
end

if response % only do this if we did not quit the trial
    
    % wait until no mouse buttons are pressed,
    % to avoid re-registering the same press
    while ~params.isQuit(keyCode) && any(buttons)
        [~,~,buttons] = GetMouse;
        [~,~,keyCode] = KbCheck;
    end
    
    % wait for a press to move on to next trial
    while ~params.isQuit(keyCode) && ~any(buttons)
        [~,~,buttons] = GetMouse;
        [~,~,keyCode] = KbCheck;
    end
    
    % chosen net location
    responsePos = clickedPos*params.screenXpixels;
    
    % move the octopus if they caught it to the bucket
    if abs(trueXpos*params.screenXpixels - responsePos) < ceil(params.dotSizePix+params.lineWidth/2)
        
        nCorrect = nCorrect + 1;
        
        % move octopus
        sXY = [trueXpos.*params.screenXpixels,params.yCenter-params.nDots*params.dotSizePix*3];
        fXY = params.buckCenter;
        m = (fXY(2)-sXY(2))/(fXY(1)-sXY(1));
        c = fXY(2)-m*fXY(1);
        octPath = @(y) (y-c)/m;
        pathSampling = sXY(2):10:fXY(2);
        
        for f = 1:length(pathSampling)
            
            % draw the tenticles
            if data.trials(t,3) > 1
                Screen('FillOval', params.window, params.cueColour, dotRects);
            else
                % squares
                Screen('FillRect', params.window, params.cueColour, dotRects);
            end
            
            % draw the net and feedback
            Screen('DrawLine', params.window, params.lineColour, responsePos, ...
                params.yCenter - 2.5*params.dotSizePix,...
                responsePos, params.yCenter + 2.5*params.dotSizePix, params.lineWidth);
            Screen('FillOval', params.window, params.feedbackColour, targetRect);
            
            rect = [octPath(pathSampling(f))-size(params.octopus,2)*params.oScale,...
                pathSampling(f)-size(params.octopus,1)*params.oScale,...
                octPath(pathSampling(f))+size(params.octopus,2)*params.oScale,...
                pathSampling(f)+size(params.octopus,1)*params.oScale];
            Screen('DrawTexture', params.window, params.imTexW, [], rect, 0);
            
            if nCorrect == 1
                DrawFormattedText(params.window,['Well done, you''ve saved ' int2str(nCorrect) ' octopus!'],...
                    'center', 0.1*params.screenYpixels, params.textColour);
            else
                DrawFormattedText(params.window,['Well done, you''ve saved ' int2str(nCorrect) ' octopuses!'],...
                    'center', 0.1*params.screenYpixels, params.textColour);
            end
            
            Screen('DrawTexture', params.window, params.imTexBu, [], params.buckRect, 0);
            Screen('Flip', params.window);
            
        end
    end
    
end

end