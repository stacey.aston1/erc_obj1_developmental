% This is an example of how to get a VE from continuous response data using
% Aston et al (2021) method - https://doi.org/10.3758/s13428-021-01633-2

% path to data
dataPath = 'D:\Dropbox\ERC_Obj1_Developmental\data\';

% load some data
T = readtable([dataPath,'ERC_Developmental_Test\ERC_Developmental_Test_session_1_trials.csv']);

% get spread cue VE
spread_targets = T{strcmp(T{:,'cue_type'},'Spread'),'true_location'};
spread_responses = T{strcmp(T{:,'cue_type'},'Spread'),'response'};
[spread_r,spread_VE,spread_CTB,spread_sig] = getVals(spread_targets,spread_responses);

% get shape cue VE
shape_targets = T{strcmp(T{:,'cue_type'},'Shape'),'true_location'};
shape_responses = T{strcmp(T{:,'cue_type'},'Shape'),'response'};
[shape_r,shape_VE,shape_CTB,shape_sig] = getVals(shape_targets,shape_responses);

% get both cue VE
both_targets = T{strcmp(T{:,'cue_type'},'Both'),'true_location'};
both_responses = T{strcmp(T{:,'cue_type'},'Both'),'response'};
[both_r,both_VE,both_CTB,both_sig] = getVals(both_targets,both_responses);