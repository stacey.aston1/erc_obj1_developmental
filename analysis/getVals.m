function [r,VE,CTB,sig] = getVals(targets,responses)

% get correlation
r = corr(targets,responses,'Type','Spearman');

% get corrected VE
[coeffs,coeffsInt,residuals] = regress(responses,...
    [targets,ones(length(responses),1)]);
% strength of bias
CTB = 1-coeffs(1);
% is the bias significantly different from zero?
sig = 1-coeffsInt(1,2) > 0;
% sensory VE
% correct if bias is significant and behavioural precision is not
% perfect
if sig && std(residuals)
    VE = std(residuals)/coeffs(1);
else
    VE = std(residuals);
end

end