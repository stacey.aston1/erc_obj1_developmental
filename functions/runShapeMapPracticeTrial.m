function data = runShapeMapPracticeTrial(params,data,t)

% wait until no mouse buttons are pressed,
% to avoid re-registering the same press
[mx,~,buttons] = GetMouse;
[~,~,keyCode] = KbCheck;
while ~params.isQuit(keyCode) && any(buttons)
    [mx,~,buttons] = GetMouse;
    [~,~,keyCode] = KbCheck;
end

% a variable that keeps track of whether there has been a response yet
response = 0; % 0 = no response, 1 = response but no feedback yet, 2 = trial over

% keep reading the mouse and updating the screen until there has been a
% response
tic;
while response < 2 && ~params.isQuit(keyCode)
    
    % get the dot positions
    dotXpos = data.sMapPracTrials(t,4:3+params.nDots);
    dotYpos = data.sMapPracTrials(t,4+params.nDots:3+params.nDots*2);
    
    % make the dot rects
    dotRects = nan(4,length(dotXpos));
    bs = data.sMapPracTrials(t,4+2*params.nDots:3+3*params.nDots); % vertical axes
    as = (params.dotSizePix^2)./bs; % horizontal axes
    for l = 1:length(dotXpos)
        dotRects(:,l) = [dotXpos(1,l)*params.screenXpixels-as(1,l);
            dotYpos(1,l)*params.screenYpixels-bs(1,l);
            dotXpos(1,l)*params.screenXpixels+as(1,l);
            dotYpos(1,l)*params.screenYpixels+bs(1,l)];
    end
    
    % get the true position
    trueXpos = data.sMapPracTrials(t,2);
    
    % set the background colour
    Screen('FillRect', params.window, params.waterColour);
    
    % draw the tenticles
    Screen('FillOval', params.window, params.cueColour, dotRects);
    
    % show the mapping
    Screen('FillOval', params.window, params.cueColour, params.mappingRectsShape);
    
    % the net
    Screen('DrawLine', params.window, params.lineColour, mx, params.yCenter - 2.5*params.dotSizePix,...
        mx, params.yCenter + 2.5*params.dotSizePix, params.lineWidth);
    
    % the correct location (only if response is issued, i.e. we are giving
    % feedback)
    if response
        
        % record this repsonse
        data.sMapPracTrials(t,end-1) = clickedPos;
        data.sMapPracTrials(t,end) = toc;
        
        % draw the "target"
        if data.part.mapDir == 2
            trueb = max(params.shapefirstLastCues)-trueXpos*params.totalShape;
        else
            trueb = trueXpos*params.totalShape+min(params.shapefirstLastCues);
        end
        truea = (params.dotSizePix^2)./trueb;
        targetRect = [trueXpos*params.screenXpixels-truea;
            params.yCenter - 2.5*params.dotSizePix-trueb*3;
            trueXpos*params.screenXpixels+truea;
            params.yCenter - 2.5*params.dotSizePix-trueb];
        Screen('FillOval', params.window, params.feedbackColour, targetRect);
        
        % draw the ideal response
        % the net
        Screen('DrawLine', params.window, params.feedbackColour, trueXpos*params.screenXpixels, ...
            params.yCenter - 2.5*params.dotSizePix,...
            trueXpos*params.screenXpixels, params.yCenter + 2.5*params.dotSizePix, params.lineWidth);
        
        response = 2;
        
    end
    % show it
    Screen('Flip', params.window);
   
    % read the mouse and keyboard to see if we have a response or are
    % quiting
    [mx,~,buttons] = GetMouse;
    [~,~,keyCode] = KbCheck;
    if any(buttons) && ~response
        response = 1;
        clickedPos = mx/params.screenXpixels;
    end
    
end

% wait until no mouse buttons are pressed,
% to avoid re-registering the same press
while ~params.isQuit(keyCode) && any(buttons)
    [~,~,buttons] = GetMouse;
    [~,~,keyCode] = KbCheck;
end

% wait for a press to move on to next trial
while ~params.isQuit(keyCode) && ~any(buttons)
    [~,~,buttons] = GetMouse;
    [~,~,keyCode] = KbCheck;
end

end