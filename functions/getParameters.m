function [params,data] = getParameters(params,part)
% generates all task parameters and a data structure for the octopus task

% load info if session > 1
if part.session > 1
    load([part.pID,'_info.mat'])
    part.mapDir = mapDir;
    part.age = age;
    part.sex = sex;
end

% add part info to data structure
data.part = part;

% add date info to data structure
data.date = datestr(now,'dd-mmm-yyyy-HH-MM-SS');

% ----- GENERAL PARAMS -----

% distance from screen
params.distance = 50; % in cm

% screen height and width
params.height = 28.6; % in cm
params.width = 50.9; % in cm

% screen width in dov
params.widthD = 2*atand(params.width/(2*params.distance));

% how many pixels per degree?
params.pixelsPerDegree = params.screenXpixels/params.widthD;

params.dotSizePix = 0.4*params.pixelsPerDegree; % radius (in degrees and swapped to pixels striaght away)
params.targetSizePix = 0.4*params.pixelsPerDegree;
params.cueGap = params.dotSizePix*1.5;
params.lineWidth = 10; % pixels (line as in the line of the net, colour defined below)
params.nDots = 8; % # likelihood dots (must be a multiple of 2)
if part.demo
    params.nLocationsToTest = 0.15:0.1:0.85; % the n true octopus locations, each to be tested m times, in screen proportions
    params.nRepeats = 2; % repeats in main trials
    params.nPracRepeats = 1; % repeats in practice trials
else
    params.nLocationsToTest = 0.15:0.02:0.85; % the n true octopus locations, each to be tested m times, in screen proportions
    params.nRepeats = 5; % repeats in main trials
    params.nPracRepeats = 2; % repeats in practice trials
end
params.nTrialsPerCueType = length(params.nLocationsToTest)*params.nRepeats;
params.nPracTrials = length(params.nLocationsToTest)*params.nPracRepeats;

% ----- SHAPE CUE -----

if part.mapDir == 1
    params.shapefirstLastCues = [0.25,0.64].*params.pixelsPerDegree;
else
    params.shapefirstLastCues = [0.64,0.25].*params.pixelsPerDegree;
end
params.totalShape = abs(params.shapefirstLastCues(1)-params.shapefirstLastCues(2));

% ----- CUE POSITIONS -----

% y Pos of dots (location and both cues)
params.yDots = zeros(params.nDots,1);
params.yDots(1,1) = params.yCenter-150-1.5*params.cueGap/2-3*params.dotSizePix;
params.yDots(2,1) = params.yCenter-150-0.5*params.cueGap/2-params.dotSizePix;
params.yDots(3,1) = params.yCenter-150+0.5*params.cueGap/2+params.dotSizePix;
params.yDots(4,1) = params.yCenter-150+1.5*params.cueGap/2+3*params.dotSizePix;
params.yDots(5,1) = params.yCenter-150+2.5*params.cueGap/2+5*params.dotSizePix;
params.yDots(6,1) = params.yCenter-150+3.5*params.cueGap/2+7*params.dotSizePix;
params.yDots(7,1) = params.yCenter-150-2.5*params.cueGap/2-5*params.dotSizePix;
params.yDots(8,1) = params.yCenter-150-3.5*params.cueGap/2-7*params.dotSizePix;

% x Pos of dots (novel cue)
params.xDots = zeros(params.nDots,1);
params.xDots(4,1) = params.xCenter-1.5*params.cueGap-3*params.dotSizePix;
params.xDots(3,1) = params.xCenter-0.5*params.cueGap-params.dotSizePix;
params.xDots(2,1) = params.xCenter+0.5*params.cueGap+params.dotSizePix;
params.xDots(1,1) = params.xCenter+1.5*params.cueGap+3*params.dotSizePix;
params.xDots(5:8,1) = params.xDots(1:4,1);

% y Pos of dots (novel cue)
params.novelYdots = [ones(1,4)*0.8*params.screenYpixels,...
    ones(1,4)*0.8*params.screenYpixels+1.5*params.cueGap+3*params.dotSizePix]./params.screenYpixels;

% The SD of the location likelihood is in degrees of visual angle (then
% converted to screen proportions)
params.locLikelihood = 6;
params.locLikelihood = (params.locLikelihood*params.pixelsPerDegree)/params.screenXpixels;
% The SD of the shape likelihood is in units of oval height (vertical axis - b)
params.shapeLikelihood = 0.18;

% ----- SET UP TRIALS -----

% create space for all trial info (trial type: 1 == native, 2 == novel, 3 ==
% both)
% cols are: trial number | true xPos (mean) | trial type | ... COLUMNS 1:3
% dot 1 xPos | ... | dot nDots xPos | ... COLUMNS 4:3+g.nDots
% dot 1 yPos | ... | dot nDots yPos | ... COLUMNS 4+g.nDots:3+2*g.nDots
% dot 1 b | ... | dot nDots b | ... COLUMNS 4+2*g.nDots:3+3*g.nDots
% response xPos (all in screen proportions) | RT | COLUMNS end-1:end
data.trials = nan(params.nTrialsPerCueType*3,params.nDots*3+5);
data.trials(:,1) = 1:params.nTrialsPerCueType*3;

% get all trial parameters
for cueType = 1:3
    
    % store the true locations
    data.trials(1+(cueType-1)*params.nTrialsPerCueType:...
        params.nTrialsPerCueType+(cueType-1)*params.nTrialsPerCueType,2) = ...
        repmat(params.nLocationsToTest',[params.nRepeats,1]);
    
    % store the trial type
    data.trials(1+(cueType-1)*params.nTrialsPerCueType:...
        params.nTrialsPerCueType+(cueType-1)*params.nTrialsPerCueType,3) = cueType;
    
    for t = 1:params.nTrialsPerCueType
        
        % random dot positions if location cue or both cues
        if cueType ~= 2
            
            % get dot positions
            dotPositions = getDotPositions(data.trials(t+(cueType-1)*params.nTrialsPerCueType,2),params);
            
            % store the dot positions
            data.trials(t+(cueType-1)*params.nTrialsPerCueType,4:3+params.nDots) = dotPositions;
            
            % also the vertical positions
            data.trials(t+(cueType-1)*params.nTrialsPerCueType,4+params.nDots:3+params.nDots*2) = params.yDots./params.screenYpixels;
            
        else % fixed positions otherwise
            
            % store the dot positions
            data.trials(t+(cueType-1)*params.nTrialsPerCueType,4:3+params.nDots) = params.xDots./params.screenXpixels;
            
            % also the vertical positions
            data.trials(t+(cueType-1)*params.nTrialsPerCueType,4+params.nDots:3+params.nDots*2) = params.novelYdots;
            
        end
        
        % shape cue generation
        if cueType > 1
            
            % get shapes
            bs = getShapes(data.trials(t+(cueType-1)*params.nTrialsPerCueType,2),params);
            
            % store them
            if part.mapDir == 2
                data.trials(t+(cueType-1)*params.nTrialsPerCueType,4+2*params.nDots:3+3*params.nDots) = ...
                    max(params.shapefirstLastCues)-bs;
            else
                data.trials(t+(cueType-1)*params.nTrialsPerCueType,4+2*params.nDots:3+3*params.nDots) = ...
                    bs+min(params.shapefirstLastCues);
            end
            
        else
            
            % or all the same size
            data.trials(t+(cueType-1)*params.nTrialsPerCueType,4+2*params.nDots:3+3*params.nDots) = params.dotSizePix;
            
        end
        
    end
    
end

% jiggle them all up
tempTrialData = squeeze(data.trials(:,2:end));
data.trials(:,2:end) = tempTrialData(randperm(size(data.trials,1)),:);

% ----- SET UP PRACTICE TRIALS

% shape practice trials (no mapping)
data.sPracTrials = nan(params.nPracTrials,params.nDots*3+5);
data.sPracTrials(:,1) = 1:params.nPracTrials;

% store the true locations
data.sPracTrials(:,2) = repmat(params.nLocationsToTest',[params.nPracRepeats,1]);

% store the trial type
data.sPracTrials(:,3) = 2;

for t = 1:params.nPracTrials
    
    % store the dot positions
    data.sPracTrials(t,4:3+params.nDots) = params.xDots./params.screenXpixels;
    
    % also the vertical positions
    data.sPracTrials(t,4+params.nDots:3+params.nDots*2) = params.novelYdots;
    
    % get shapes
    bs = getShapes(data.sPracTrials(t,2),params);
    
    % store them
    if part.mapDir == 2
        data.sPracTrials(t,4+2*params.nDots:3+3*params.nDots) = ...
            max(params.shapefirstLastCues)-bs;
    else
        data.sPracTrials(t,4+2*params.nDots:3+3*params.nDots) = ...
            bs+min(params.shapefirstLastCues);
    end
    
end

% jiggle them all up
tempTrialData = data.sPracTrials(:,2:end);
data.sPracTrials(:,2:end) = tempTrialData(randperm(size(data.sPracTrials,1)),:);

% the colour cue practice trials (with mapping)
data.sMapPracTrials = nan(params.nPracTrials,params.nDots*3+5);
data.sMapPracTrials(:,1) = 1:params.nPracTrials;

% store the true locations
data.sMapPracTrials(:,2) = repmat(params.nLocationsToTest',[params.nPracRepeats,1]);

% store the trial type
data.sMapPracTrials(:,3) = 2;

for t = 1:params.nPracTrials
    
    % store the dot positions
    data.sMapPracTrials(t,4:3+params.nDots) = params.xDots./params.screenXpixels;
    
    % also the vertical positions
    data.sMapPracTrials(t,4+params.nDots:3+params.nDots*2) = params.novelYdots;
    
    % get shapes
    bs = getShapes(data.sMapPracTrials(t,2),params);
    
    % store them
    if part.mapDir == 2
        data.sMapPracTrials(t,4+2*params.nDots:3+3*params.nDots) = ...
            max(params.shapefirstLastCues)-bs;
    else
        data.sMapPracTrials(t,4+2*params.nDots:3+3*params.nDots) = ...
            bs+min(params.shapefirstLastCues);
    end
    
end

% jiggle them all up
tempTrialData = data.sMapPracTrials(:,2:end);
data.sMapPracTrials(:,2:end) = tempTrialData(randperm(size(data.sMapPracTrials,1)),:);
    
% ----- SET UP THE INTRO TRIALS -----
% make these a random set of trials from the main trials

% loc cue intro trials
theseTrials = find(data.trials(:,3) == 1);
randomList = randperm(length(theseTrials));
data.lIntroTrials = data.trials(theseTrials(randomList(1:5)),:);

% shape cue intro trials
theseTrials = find(data.trials(:,3) == 2);
randomList = randperm(length(theseTrials));
data.sIntroTrials = data.trials(theseTrials(randomList(1:5)),:);

% loc shape cues intro trials
theseTrials = find(data.trials(:,3) == 3);
randomList = randperm(length(theseTrials));
data.lsIntroTrials = data.trials(theseTrials(randomList(1:5)),:);

% ----- GET THE SHAPE MAPPING -----

params.mappingLocs = 0.14:0.04:0.86;
params.mappingShape = (params.mappingLocs.*params.totalShape)+min(params.shapefirstLastCues);
if part.mapDir == 2
    params.mappingShape = fliplr(params.mappingShape);
end
as = (params.dotSizePix^2)./params.mappingShape;
params.mappingRectsShape = nan(4,length(params.mappingLocs));
counter = 0;
for l = 1:length(params.mappingLocs)
    counter = counter + 1;
    params.mappingRectsShape(:,counter) = [...
        params.mappingLocs(1,l)*params.screenXpixels-as(1,counter);
        params.yCenter-params.mappingShape(1,counter);
        params.mappingLocs(1,l)*params.screenXpixels+as(1,counter);
        params.yCenter+params.mappingShape(1,counter)];
end

% ----- PREPARE THE IMAGES -----

% prepare the octopus images
[im,map] = imread('whiteOctopus.png','png');
im = ind2rgb(im,map);
im = double(im)/255;
im = im./max(im(:));
imW = size(im,2);
imH = size(im,1);
im = reshape(im,[size(im,1)*size(im,2),3]);
octInds = find(im(:,1));
im(octInds,:) = repmat(params.cueColour,length(octInds),1);
backInds = find(~im(:,1));
im(backInds,:) = repmat(params.waterColour,length(backInds),1);
params.octopus = reshape(im,[imH,imW,3]);

% prepare the bucket images
[im,map] = imread('bucket.png','png');
im = ind2rgb(im,map);
im = double(im)/255;
im = im./max(im(:));
imW = size(im,2);
imH = size(im,1);
im = reshape(im,[size(im,1)*size(im,2),3]);
backInds = find(im(:,1)==1);
otherInds = find(im(:,1)~=1);
im(otherInds,:) = im(otherInds,:)*0.4;
im(backInds,:) = repmat(params.waterColour,length(backInds),1);
params.bucket = reshape(im,[imH,imW,3]);

% where to put the bucket
buckScaleFac = 0.15;
params.buckRect = [params.xCenter-size(params.bucket,2)*buckScaleFac,...
    params.screenYpixels*0.8-size(params.bucket,1)*2.5*buckScaleFac,...
    params.xCenter+size(params.bucket,2)*buckScaleFac,...
    params.screenYpixels*0.8-size(params.bucket,1)*0.5*buckScaleFac];
params.buckCenter = [params.xCenter,params.screenYpixels*0.8-1.5*buckScaleFac*size(params.bucket,1)];

% octopus scale factor
params.oScale = 0.25;











