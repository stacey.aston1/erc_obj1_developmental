function bs = getShapes(m,params)

% generate shapes
bs = randn(1,params.nDots)*params.shapeLikelihood+m*params.totalShape;

% scale the SD
temp_bs = mean(bs);
bs = bs - temp_bs;
bs = (bs./std(bs))*params.shapeLikelihood;
bs = bs + temp_bs;

% check that they don't fall off the screen
while any(bs < 0) || any(bs > params.totalShape)
    bs = randn(1,params.nDots)*params.shapeLikelihood+m*params.totalShape;
    temp_bs = mean(bs);
    bs = bs - temp_bs;
    bs = (bs./std(bs))*params.shapeLikelihood;
    bs = bs + temp_bs;
end
            
end