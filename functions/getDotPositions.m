function dotPositions = getDotPositions(m,params)

% generate dot positions (need a little while loop to check the
% centroid of the octopus doesnt stray too far to the edge of the
% screen so that a tenticle falls off the screen)
dotPositions = randn(1,params.nDots)*params.locLikelihood+m;

% scale the SD
tempMeanDotPosition = mean(dotPositions);
dotPositions = dotPositions - tempMeanDotPosition;
dotPositions = (dotPositions./std(dotPositions))*params.locLikelihood;
dotPositions = dotPositions + tempMeanDotPosition;

% check that they don't fall off the screen
while any(dotPositions < params.dotSizePix/params.screenXpixels) || ...
        any(dotPositions > 1-params.dotSizePix/params.screenXpixels)
    dotPositions = randn(1,params.nDots)*params.locLikelihood+m;
    tempMeanDotPosition = mean(dotPositions);
    dotPositions = dotPositions - tempMeanDotPosition;
    dotPositions = (dotPositions./std(dotPositions))*params.locLikelihood;
    dotPositions = dotPositions + tempMeanDotPosition;
end

end