% Clear the workspace
clc; clear; close all;
sca;

%% ----- DEPENDENCIES -----

% path to parent folder
params.parentFolder = [pwd,'\'];

% add path to the functions we need
addpath(genpath([params.parentFolder,'functions']))

%% ----- SET UP PSYCHTOOLBOX -----

% Some default settings
PsychDefaultSetup(2);
Screen('Preference', 'SkipSyncTests', 1);

% Random number generator
rng('shuffle');

% Get the screen numbers. This gives us a number for each of the screens
% attached to our computer.
params.screens = Screen('Screens');
  
% Defines the screen to use
params.screenNumber = max(params.screens);

% Open a window
[params.window, params.windowRect] = PsychImaging('OpenWindow', params.screenNumber);

% Set up alpha-blending for smooth (anti-aliased) lines
Screen('BlendFunction', params.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% Get the size of the on screen window in pixels
[params.screenXpixels, params.screenYpixels] = Screen('WindowSize', params.window);

% Get the centre coordinate of the window in pixels
[params.xCenter, params.yCenter] = RectCenter(params.windowRect);

% set up a quit button
KbName('UnifyKeyNames');
params.keyIDs{1,1} = 'quit';
params.keyInd(1,1) = KbName('ESCAPE'); 
params.keyIDs{2,1} = 'continue';
params.keyInd(2,1) = KbName('SPACE'); 

% set up keyCode checks
params.isQuit = @(keyCode) keyCode(params.keyInd(1,1));
params.isContinue = @(keyCode) keyCode(params.keyInd(2,1));

% Hide the cursor   
HideCursor;

% text settings
Screen('TextFont', params.window, 'Ariel');
Screen('TextSize', params.window, 30);

% colours
params.cueColour = [0.5 0.5 0.5];
params.waterColour = [0.1 0.2 0.4];
params.lineColour = [0 0 0];
params.textColour = [0.7 0.7 0.7];
params.feedbackColour = [0.1 0.8 0.1];

%% ----- GET INFORMATION ABOUT THE PARTICIPANT -----

Screen('FillRect', params.window, params.waterColour);
part.pID = ['ERC_Developmental_',GetEchoString(params.window, 'Enter Participant Number (1-99):',...
    params.screenXpixels*0.1,params.screenYpixels*0.35,params.textColour,params.waterColour,0,2,[],[])];
part.session = str2double(GetEchoString(params.window, 'Session Number (1 or 2):',...
     params.screenXpixels*0.1,params.screenYpixels*0.45,params.textColour,params.waterColour,0,2,[],[]));
part.demo = str2double(GetEchoString(params.window, 'Run demo (1 = yes, 0 = no):',...
     params.screenXpixels*0.1,params.screenYpixels*0.55,params.textColour,params.waterColour,0,2,[],[]));
part.date = datestr(now);

%% ----- GET EXTRA INFORMATION IF THIS IS THE FIRST SESSION -----

if part.session == 1
    Screen('FillRect', params.window, params.waterColour);
    % get age
    part.age = str2double(GetEchoString(params.window, 'Enter Participant Age (in years):',...
        params.screenXpixels*0.1,params.screenYpixels*0.35,params.textColour,params.waterColour,0,2,[],[]));
    % check the information that has been entered
    if ~any(part.age == 18:40)
        sca; error('Age must be in the range 18 to 40');
    end
    % get sex
    part.sex = GetEchoString(params.window, 'Enter Participant Sex At Birth (Female, Male, Don''t Know, or Prefer Not To Say):',...
        params.screenXpixels*0.1,params.screenYpixels*0.45,params.textColour,params.waterColour,0,2,[],[]);
    % check the information that has been entered
    if ~ismember(part.sex,{'Female','Male','Don''t Know','Prefer Not To Say'})
        sca; error('Sex must be one of Female, Male, Don''t Know, or Prefer Not To Say');
    end
    % randomly decide an audio direction for this P
    part.mapDir = randi(2);
    % make a directories for this P
    mkdir([params.parentFolder,'data\',part.pID])
    mkdir([params.parentFolder,'memoryDumps\',part.pID])
end
 
%% ----- GET TASK PARAMETERS -----

% get the paramters and trial data
[params,data] = getParameters(params,part);
  
% make the textures
params.imTexW = Screen('MakeTexture', params.window, params.octopus);
params.imTexBu = Screen('MakeTexture', params.window, params.bucket);

%% ----- WELCOME SCREEN -----

welcomeText = ['Welcome! In this task you will learn how to use a new cue to\n\n',...
    'location. The first set of trials in the experiment will teach you\n\n',...
    'how to use this new cue. \n\n\n',...
    'Press space to continue.'];
textBlock(params,welcomeText);

%% ----- PRACTICE WITH MAPPING -----
    
% shape cue mapping practice
mapText = ['In these trials, you will see eight ovals at the bottom\n\n',...
    'of the screen. Your task is to estimate the average stretch and orientation of \n\n',...
    'the ovals, using the mouse to indicate your estimate across the examples\n\n',...
    'by positioning the black vertical line. A left mouse click will register\n\n',...
    'your response and you will be shown feedback and a red vertical line \n\n',...
    'indicating the ideal response. A second left click will take you to \n\n',...
    'the next trial. If you have any questions, or if anything is not clear, \n\n',...
    'please ask the experimenter now. \n\n\n',...
    'Press space to continue.'];
textBlock(params,mapText);

for t = 1:params.nPracTrials
    data = runShapeMapPracticeTrial(params,data,t);
end

%% ----- PRACTICE WITHOUT MAPPING -----
    
noMapText = ['In this next series of trials, you are required to complete the\n\n',...
    'same task. This time, however, the mapping will not be shown on\n\n',...
    'the screen so you must remember the location each shape corresponds to. \n\n\n',...
    'Press space to continue.'];
textBlock(params,noMapText);

for t = 1:params.nPracTrials
    data = runShapePracticeTrial(params,data,t);
end

%% ----- DEMONSTRATION TRIALS -----

% do the novel cue trial practice
novelPracticeText = ['You will now begin the main task where you will begin to use\n\n',...
    'the new cue to location to find octopi hiding in the sea. If you\n\n',...
    'catch the octopus, it will go in your bucket. Here are some examples\n\n',...
    'of trials where the new cue is the only cue to location. See if you can catch\n\n',...
    'the octopi by placing the vertical black line where you think they are. \n\n\n',...
    'Press space to continue.'];
textBlock(params,novelPracticeText);

for t = 1:5
    trialNumber = data.sIntroTrials(t,1);
    tempData = runMainTrial(params,data,0,trialNumber);
    data.sIntroTrials(t,end-1:end) = tempData.trials(t,end-1:end);
end

% do the loc cue practice trials
nativePracticeText = ['In some trials, some will use a more familiar spatial cue to the \n\n',...
    'octopi location. The eight markers will be a sample of the octopus'' \n\n',...
    'true location, but note that only horizontal position matters. If you\n\n',...
    'catch the octopus, it will go in your bucket. Here are some examples\n\n',...
    'of trials where only the spatial cue is present. See if you can catch\n\n',...
    'the octopi by placing the vertical black line where you think they are. \n\n\n',...
    'Press space to continue.'];
textBlock(params,nativePracticeText);

for t = 1:5
    tempData = runMainTrial(params,data,0,data.lIntroTrials(t,1));
    data.lIntroTrials(t,end-1:end) = tempData.trials(t,end-1:end);
end

% do practice trials for both cues
bothPracticeText = ['Finally, in some trials, you will have both the spatial and novel\n\n',...
    'cue to the octopus'' location. Here are some examples of trials with both\n\n',...
    'cues. See if you can catch the octopi this time. \n\n\n',...
    'Press space to continue.'];
textBlock(params,bothPracticeText);

for t = 1:5
    trialNumber = data.lsIntroTrials(t,1);
    tempData = runMainTrial(params,data,0,trialNumber);
    data.lsIntroTrials(t,end-1:end) = tempData.trials(t,end-1:end);
end

%% ----- MAIN TRIALS -----

% main task instructions
mainText = ['Great! You are now ready to begin the main task where these three\n\n',...
    'types of trials will be interleaved. Good luck catching those octopi!! \n\n\n',...
    'Press space to continue.'];
textBlock(params,mainText);

nCorrect = 0;
t = 0;
[~,~,keyCode] = KbCheck;
while t < size(data.trials,1) && ~params.isQuit(keyCode)
    % next trial
    t = t + 1;
    [data,nCorrect] = runMainTrial(params,data,nCorrect,t);
end

%% -------------------- Final screen --------------------------------------

finishedText = ['Thanks! The task is now complete. We are very grateful for your time. \n\n\n',...
    'Press space to continue.'];
textBlock(params,finishedText);

% format and output main data
true_location = data.trials(:,2);
cue_type = cell(length(true_location),1);
cue_type(data.trials(:,3) == 1,1) = {'Spread'};
cue_type(data.trials(:,3) == 2,1) = {'Shape'};
cue_type(data.trials(:,3) == 3,1) = {'Both'};
dot_1_location = data.trials(:,4);
dot_2_location = data.trials(:,5);
dot_3_location = data.trials(:,6);
dot_4_location = data.trials(:,7);
dot_5_location = data.trials(:,8);
dot_6_location = data.trials(:,9);
dot_7_location = data.trials(:,10);
dot_8_location = data.trials(:,11);
dot_1_shape_location = abs(data.trials(:,20) - params.shapefirstLastCues(1,1))./params.totalShape;
dot_2_shape_location = abs(data.trials(:,21) - params.shapefirstLastCues(1,1))./params.totalShape;
dot_3_shape_location = abs(data.trials(:,22) - params.shapefirstLastCues(1,1))./params.totalShape;
dot_4_shape_location = abs(data.trials(:,23) - params.shapefirstLastCues(1,1))./params.totalShape;
dot_5_shape_location = abs(data.trials(:,24) - params.shapefirstLastCues(1,1))./params.totalShape;
dot_6_shape_location = abs(data.trials(:,25) - params.shapefirstLastCues(1,1))./params.totalShape;
dot_7_shape_location = abs(data.trials(:,26) - params.shapefirstLastCues(1,1))./params.totalShape;
dot_8_shape_location = abs(data.trials(:,27) - params.shapefirstLastCues(1,1))./params.totalShape;
response = data.trials(:,end-1);
reaction_time = data.trials(:,end);
T = table(true_location,cue_type,dot_1_location,dot_2_location,dot_3_location,...
    dot_4_location,dot_5_location,dot_6_location,dot_7_location,dot_8_location,...
    dot_1_shape_location,dot_2_shape_location,dot_3_shape_location,dot_4_shape_location,...
    dot_5_shape_location,dot_6_shape_location,dot_7_shape_location,dot_8_shape_location,...
    response,reaction_time);
writetable(T,[params.parentFolder,'data\',part.pID,'\',part.pID,'_session_',...
    num2str(part.session),'_trials.csv']);

% dump everything (just incase)
save([params.parentFolder,'memoryDumps\',part.pID,'\',part.pID,'_',...
    num2str(part.session),'_',datestr(now,'dd-mmm-yyyy-HH-MM-SS')])

% Clear the screen
sca;














